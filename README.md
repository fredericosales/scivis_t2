# Title


## subtitle


### Frederico Sales
### <frederico.sales@engenharia.ufjf.br>
### 2018

# link
[google](http://google.com)


## table

| here | comes | the | pain |
|:----:|:-----:|:---:|:----:|
| some |thing  |to   | tell |


# List

## simple list

1. here
2. comes 
3. the
4. pain

## another list

* here
* comes 
* the
* pain

## fence list

	(i)   here
	(ii)  comes
	(iii) the
	(iv)  pain

# Another bites the dust

> here we are

>  again some rando text

> dude you did it again...

# some fence stuff

__text__
_text_

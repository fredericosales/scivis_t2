#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
from __init__ import *


if __name__ == "__main__":
    # do some:
    
    for i in range(0, 61):
    	i = (i / 10)
    	sca = Scalar(t=i)

    	print("tempo: {}\t\t velocidade: {:.4f}\t\t posicao: {:.4f}".format(i*10, sca.velocidade(), sca.posicao()))

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
import numpy as numpy
import math
import simpy


# main
class Scalar(object):
	"""
	SciVis
	Frederico Sales
	Iago Rosa
	2018.3
	"""

	def __init__(self, m=2, v0=100, p0=0, R=1.2, t=0):
		"""
		F = -R dx/dt
		T = m/R
		m d^2x/dt^2 = -R dx/dt

		Cx determinada por valor inicial x quando t = 0
		chamaremos de x0.

		x = x0 + v0 * m / R [1- exp((-R/m))*t]
		v = v0 * exp(-R/m * t)
		p = p0 + ((v0 * m) / R) * (1 - exp((-R/m))*t) 

		:param m: massa [Kg]
		:param vi: V0 velocidade inicial [m/s]
		:param pi: P0 posição inicial [m]
		:param r: constante da forca R [N s/m]
		:param t: tempo [s]
		"""
		self.m = m
		self.v0 = v0
		self.p0 = p0
		self.R = R
		self.t = t
	
	def velocidade(self):
		"""
		Calcula velocidade para o t = 0.
		:return: velocidade
		"""
		vel = self.v0 * math.exp(-(self.R / self.m)* self.t) 
		return vel

	def posicao(self, t=0):
		"""
		Retorna posicao em funcao do tempo
		:param t: tempo
		:return: posicao
		"""
		pos = self.p0 + ((self.v0 * self.m) / self.R) * (1 - math.exp(-(self.R / self.m) * self.t))
		return pos
